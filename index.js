const sqlite3 = require('sqlite3')
const bcrypt = require('bcrypt')
const morgan = require('morgan')
const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const path = require('path')

const port = process.env.PORT || 8080
const app = express()

sqlite3.verbose()

// Stores users permanently
const userDatabase = new sqlite3.Database(path.join(__dirname, 'users.sqlite'))

// Stores sessions temporarily
const activeSessions = new sqlite3.Database(':memory:')

// SESSION STATEMENTS

const SESSION_TABLE_CREATE =
	'CREATE TABLE if not exists session_list (id INTEGER PRIMARY KEY AUTOINCREMENT, session TEXT NOT NULL UNIQUE, user TEXT NOT NULL UNIQUE, expiry DATETIME NOT NULL);'
const SESSION_TABLE_DROP = 'DROP TABLE if exists session;'
const SESSION_SELECT = 'SELECT * FROM [session_list] WHERE session = ?;'
const SESSION_REFRESH =
	'INSERT OR REPLACE INTO [session_list] (session, user, expiry) VALUES (?,?,?);'

//USER STATEMENTS

const USER_INSERT = 'INSERT INTO [account] (username, password) VALUES (?,?);'
const USER_SELECT = 'SELECT * FROM [account] WHERE username = ?;'
const USER_LIST = 'SELECT username FROM [account];'

async function insertUser(username, password, salt = 10) {
	const account = await new Promise((resolve, reject) => {
		userDatabase.get(USER_SELECT, username.toLowerCase(), (err, row) => {
			if (err) {
				return reject(err)
			}

			return resolve(row)
		})
	})

	if (account) {
		return false
	}

	return new Promise(async (resolve, reject) => {
		userDatabase.run(
			USER_INSERT,
			username.toLowerCase(),
			await bcrypt.hash(password.toString(), salt),
			err => {
				if (err) {
					return reject(err)
				}
				return resolve(true)
			},
		)
	})
}

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(
	session({
		secret: 'something_secret',
		cookie: { maxAge: 5 * 60000 },
	}),
)

async function loadSession(req, res, next) {
	req.loadedSession = await new Promise((resolve, reject) => {
		activeSessions.get(SESSION_SELECT, req.session.id, (err, row) => {
			if (err) {
				return reject(err)
			}
			return resolve(row)
		})
	})

	next()
}

function checkSession(req, res, next) {
	if (req.loadedSession?.session === req.session.id) {

        //TODO check session for expiry

		return next()
	}

	res.redirect('/login')
}

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, 'welcome.html'))
})

app.get('/login', (req, res) => {
	res.sendFile(path.join(__dirname, 'login.html'))
})

app.get('/main', loadSession, checkSession, (req, res) => {
	res.write(
		'<h1>Welcome ' +
			req.loadedSession.user +
			'</h1><p>session id: ' +
			req.session.id +
			'</p>',
	)
	res.write('<a href="/new_user">Create new user</a>')
	res.write('</br>')
	res.write('<a href="/list_users">List users</a>')
	res.write('</br>')
	res.write(
		'<a href="/datetimenow">WITH THIS LINK YOU CAN SEE THE TIME RIGHT NOW</a>',
	)
	res.end()
})

app.get('/new_user', loadSession, checkSession, (req, res) => {
	res.sendFile(path.join(__dirname, 'new_user.html'))
})

app.post('/create_user', loadSession, checkSession, async (req, res) => {
	if (!req.body) {
		return res.status(400).json({ error: 'No data' })
	}

	if (!req.body.username) {
		return res.status(400).json({ error: 'Username missing' })
	}

	if (!req.body.password) {
		return res.status(400).json({ error: 'A password is missing' })
	}

	if (req.body.password != req.body.password_repeat) {
		return res.status(400).json({ error: 'Passwords do not match' })
	}

	const result = await insertUser(req.body.username, req.body.password)

	if (result) {
		return res.status(200).json({ result: 'User created' })
	}

	return res.status(409).json({ error: 'Username already exists' })
})

app.get('/list_users', loadSession, checkSession, async (req, res) => {
	const result = await new Promise((resolve, reject) => {
		userDatabase.all(USER_LIST, (err, row) => {
			if (err) {
				return reject(err)
			}
			return resolve(row)
		})
	})

	if (result) {
		return res.status(200).json(result)
	}

	return res.status(400).json({ error: 'No data' })
})

app.post('/auth_user', loadSession, async (req, res) => {
	// Check if username and password exist
	if (!req.body?.username || !req.body.password) {
		return res.status(400).json({ error: 'Missing username or password' })
	}

	// Destructure username and password for use in queries
	let { username, password } = req.body

    username = req.body.username.toLowerCase()

	const account = await new Promise((resolve, reject) => {
		userDatabase.get(USER_SELECT, username, (err, row) => {
			if (err) {
				return reject(err)
			}

			return resolve(row)
		})
	})

	if (account?.username !== username) {
		return res.status(401).json({ error: 'Unknown user' })
	}

	if (await bcrypt.compare(password, account.password)) {
		await new Promise((resolve, reject) => {
			activeSessions.run(
				SESSION_REFRESH,
				req.session.id,
				username,
				new Date(new Date() + 5 * 60000),
				err => {
					if (err) {
						return reject(err)
					}

					return resolve()
				},
			)
		})

		return res.redirect('/main')
	}

	return res.status(401).json({ error: 'Login failed' })
})

app.get('/datetimenow', loadSession, checkSession, (req, res) => {
	res.send('The time is now: ' + new Date().toISOString())
})

async function StartUp() {
	await new Promise((resolve, reject) => {
		activeSessions.run(SESSION_TABLE_DROP, err => {
			if (err) {
				reject(err)
			}

			resolve()
		})
	})

	await new Promise((resolve, reject) => {
		activeSessions.run(SESSION_TABLE_CREATE, err => {
			if (err) {
				reject(err)
			}

			resolve()
		})
	})

	app.listen(port, () => {
		console.log('Server started on port:', port)
	})
}

StartUp()
