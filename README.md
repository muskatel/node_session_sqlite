# User Login Example

- Active Sessions Stored using SQLite3 in memory
- Create User
- List users
- See the time !!!

## TODO

- Returning users

## Usage

1) Install dependancies

```bash
npm install
```

2) Launch using start script

```bash
npm start
```
## Authors

Craig Marais (@muskatel)

Greg Linllater (@EternalDeiwos)

## License

MIT License

Copyright (c) 2022 Craig Marais

See LICENSE file
